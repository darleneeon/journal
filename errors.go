package journal

// General errors.
const (
	ErrInternal = Error("internal error")
)

// Notes errors.
const (
	ErrNoteRequired     = Error("note required")
	ErrNoteNotFound     = Error("note not found")
	ErrNoteIDRequired   = Error("note id required")
	ErrNoteTextRequired = Error("note text required")
)

// Password errors.
const (
	ErrWrongPassword       = Error("wrong password")
	ErrPasswordRequired    = Error("password required")
	ErrNewPasswordRequired = Error("new password required")
	ErrPasswordsRequired   = Error("passwords required")
	ErrEqualPasswords      = Error("passwords should not be equal")
	ErrPasswordNotSet      = Error("password not set")
)

// Error represents a journal error.
type Error string

// Error returns the error message.
func (e Error) Error() string { return string(e) }
