package encryption

import (
	"gitlab.com/darleneeon/journal"
	"golang.org/x/crypto/bcrypt"
)

// Ensure EncryptionService implements journal.EncryptionService
var _ journal.EncryptionService = &EncryptionService{}

// EncryptionService represents a service for encrypting Notes.
type EncryptionService struct{}

// GeneratePasswordHash returns the bcrypt hash of the password.
func (s *EncryptionService) GeneratePasswordHash(password string) ([]byte, error) {
	hash, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		return nil, err
	}

	return hash, nil
}

// CompareHashAndPassword compares a bcrypt hashed password with its
// possible plaintext equivalent. Returns nil on success, or an error on failure.
func (s *EncryptionService) CompareHashAndPassword(hash []byte, password string) error {
	err := bcrypt.CompareHashAndPassword(hash, []byte(password))
	if err == bcrypt.ErrMismatchedHashAndPassword {
		return journal.ErrWrongPassword
	}

	return err
}

// EncryptNote encrypts the text of the Note by the password.
func (s *EncryptionService) EncryptNote(Note *journal.Note, password string) error {
	if Note.Encrypted {
		return nil
	}

	cryptoText, err := encryptText(Note.Text, password)
	if err != nil {
		return err
	}

	Note.Text = cryptoText
	Note.Encrypted = true

	return nil
}

// DecryptNote decrypts the text of the Note by the password.
func (s *EncryptionService) DecryptNote(Note *journal.Note, password string) error {
	if !Note.Encrypted {
		return nil
	}

	plainText, err := decryptText(Note.Text, password)
	if err != nil {
		return err
	}

	Note.Text = plainText
	Note.Encrypted = false

	return nil
}
