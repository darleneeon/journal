package encryption

import (
	"reflect"
	"testing"

	"gitlab.com/darleneeon/journal"
)

func TestGeneratePasswordHash(t *testing.T) {
	service := &EncryptionService{}

	password := "qwerty11"
	passwordHash, err := service.GeneratePasswordHash(password)
	if err != nil {
		t.Error(err)
	}

	if password == string(passwordHash) {
		t.Error("Hash matches password")
	}

	if reflect.DeepEqual(passwordHash, hashTo32Bytes(password)) {
		t.Error("GeneratePasswordHash equal to hashTo32Bytes")
	}
}

func TestCompareHashAndPassword(t *testing.T) {
	service := &EncryptionService{}

	password := "qwerty11"
	passwordHash, _ := service.GeneratePasswordHash(password)

	if err := service.CompareHashAndPassword(passwordHash, password); err != nil {
		t.Error(err)
	}
}

func TestEncryptNote_Unencrypted(t *testing.T) {
	service := &EncryptionService{}

	password := "qwerty11"
	Note := &journal.Note{
		Text: "some text",
	}

	if err := service.EncryptNote(Note, password); err != nil {
		t.Error(err)
	} else if Note.Text == "some text" {
		t.Error("Note text wasn't encrypted")
	} else if !Note.Encrypted {
		t.Error("Encryption flag wasn't set")
	}
}

func TestEncryptNote_Encrypted(t *testing.T) {
	service := &EncryptionService{}

	password := "qwerty111"
	Note := &journal.Note{
		Text:      "some text",
		Encrypted: true,
	}

	if err := service.EncryptNote(Note, password); err != nil {
		t.Error(err)
	} else if Note.Text != "some text" {
		t.Error("Note was encrypted again")
	}
}

func TestDecryptNote_Encrypted(t *testing.T) {
	service := &EncryptionService{}

	password := "qwerty11"
	Note := &journal.Note{
		Text: "some text",
	}

	service.EncryptNote(Note, password)

	if err := service.DecryptNote(Note, password); err != nil {
		t.Error(err)
	} else if Note.Text != "some text" {
		t.Error("Note text wasn't decrypted")
	} else if Note.Encrypted {
		t.Error("Encryption flag wasn't removed")
	}
}

func TestDecryptNote_Unencrypted(t *testing.T) {
	service := &EncryptionService{}

	password := "qwerty11"
	Note := &journal.Note{
		Text:      "some text",
		Encrypted: false,
	}

	if err := service.DecryptNote(Note, password); err != nil {
		t.Error(err)
	} else if Note.Text != "some text" {
		t.Error("Note text was decrypted again")
	}
}
