package bolt

import (
	"reflect"
	"sort"
	"time"

	"gitlab.com/darleneeon/journal"
	"gitlab.com/darleneeon/journal/bolt/internal"
)

// Ensure NoteService implements journal.NoteService
var _ journal.NoteService = &NoteService{}

// NoteService represents a service for managing notes.
type NoteService struct {
	client *Client
}

// Note returns a note by ID.
func (s *NoteService) Note(id journal.NoteID) (*journal.Note, error) {
	// Start read-only transaction
	tx, err := s.client.db.Begin(false)
	if err != nil {
		return nil, err
	}
	defer tx.Rollback()

	// Find and unmarshal note
	var note journal.Note
	if bn := tx.Bucket([]byte(notesBucket)).Get([]byte(id)); bn == nil {
		return nil, journal.ErrNoteNotFound
	} else if err := internal.UnmarshalNote(bn, &note); err != nil {
		return nil, err
	}

	return &note, nil
}

// Notes returns all existing notes.
func (s *NoteService) Notes() ([]journal.Note, error) {
	// Start read-only transaction
	tx, err := s.client.db.Begin(false)
	if err != nil {
		return nil, err
	}
	defer tx.Rollback()

	b := tx.Bucket([]byte(notesBucket))
	c := b.Cursor()

	// Prepare slice for notes
	notes := make([]journal.Note, 0, b.Sequence())

	// Fill the slice
	var note journal.Note
	for key, bn := c.First(); key != nil; key, bn = c.Next() {
		if err := internal.UnmarshalNote(bn, &note); err != nil {
			return []journal.Note{}, err
		}
		notes = append(notes, note)
	}

	// Sort the notes by date
	sort.Slice(notes, func(i, j int) bool {
		return notes[i].Date.After(notes[j].Date)
	})

	return notes, nil
}

// SaveNote saves a note.
func (s *NoteService) SaveNote(n *journal.Note) error {
	// Check object and ID.
	if n == nil {
		return journal.ErrNoteRequired
	} else if len(string(n.ID)) == 0 {
		return journal.ErrNoteIDRequired
	}

	// Set the date if it isn't set
	if reflect.DeepEqual(n.Date, time.Time{}) {
		n.Date = s.client.Now()
	}

	// Start read-write transaction
	tx, err := s.client.db.Begin(true)
	if err != nil {
		return err
	}
	defer tx.Rollback()

	b := tx.Bucket([]byte(notesBucket))

	// Marshal and insert the note.
	if bn, err := internal.MarshalNote(n); err != nil {
		return err
	} else if err := b.Put([]byte(n.ID), bn); err != nil {
		return err
	}

	return tx.Commit()
}

// DeleteNote deletes a note by id.
func (s *NoteService) DeleteNote(id journal.NoteID) error {
	// Start read-write transaction
	tx, err := s.client.db.Begin(true)
	if err != nil {
		return err
	}
	defer tx.Rollback()

	b := tx.Bucket([]byte(notesBucket))

	// Delete the note if it exists
	if b.Get([]byte(id)) == nil {
		return journal.ErrNoteNotFound
	} else if err := b.Delete([]byte(id)); err != nil {
		return err
	}

	return tx.Commit()
}
