package bolt_test

import (
	"reflect"
	"testing"

	"gitlab.com/darleneeon/journal"
)

// Ensure note can be saved, updated and retrieved.
func TestNoteService_SaveNote(t *testing.T) {
	c := MustOpenClient()
	defer c.Close()
	s := c.NoteService()

	note := journal.Note{
		ID:   "ID",
		Text: "TEXT",
	}

	// Save the note.
	if err := s.SaveNote(&note); err != nil {
		t.Fatal(err)
	}

	// Retrieve note and compare.
	other, err := s.Note("ID")
	if err != nil {
		t.Fatal(err)
	} else if !reflect.DeepEqual(&note, other) {
		t.Fatalf("unexpected note: %#v", other)
	}

	// Update the note.
	note.Text = "NEW TEXT"
	if err := s.SaveNote(&note); err != nil {
		t.Fatal(err)
	}

	// Retrieve updated note and compare.
	other, err = s.Note("ID")
	if err != nil {
		t.Fatal(err)
	} else if !reflect.DeepEqual(&note, other) {
		t.Fatalf("unexpected note: %#v", other)
	}
}

// Ensure note validates the id.
func TestNoteService_SaveNote_ErrNoteIDRequired(t *testing.T) {
	c := MustOpenClient()
	defer c.Close()
	if err := c.NoteService().SaveNote(&journal.Note{ID: ""}); err != journal.ErrNoteIDRequired {
		t.Fatal(err)
	}
}

// Ensure note can be deleted.
func TestNoteService_DeleteNote(t *testing.T) {
	c := MustOpenClient()
	defer c.Close()
	s := c.NoteService()

	// Save the note.
	note := journal.Note{
		ID:   "ID",
		Text: "TEXT",
	}

	s.SaveNote(&note)

	// Delete the note.
	if err := s.DeleteNote("ID"); err != nil {
		t.Fatal(err)
	}
}

// Ensure nonexistent note can not be deleted.
func TestNoteService_DeleteNote_ErrNoteNotFound(t *testing.T) {
	c := MustOpenClient()
	defer c.Close()
	s := c.NoteService()

	// Delete nonexistent note.
	if err := s.DeleteNote("ID"); err != journal.ErrNoteNotFound {
		t.Fatal(err)
	}
}

// Ensure all notes can be received.
func TestNoteService_Notes(t *testing.T) {
	c := MustOpenClient()
	defer c.Close()
	s := c.NoteService()

	// Create couple notes.
	note1 := journal.Note{
		ID:   "ID1",
		Text: "TEXT",
	}

	note2 := journal.Note{
		ID:   "ID2",
		Text: "TEXT",
	}

	s.SaveNote(&note1)
	s.SaveNote(&note2)

	// Get all notes.
	notes, err := s.Notes()
	if err != nil {
		t.Fatal(err)
	}

	expected := []journal.Note{note1, note2}
	if !reflect.DeepEqual(notes, expected) {
		t.Fatalf("unexpected slice: %v, need: %v", notes, expected)
	}
}
