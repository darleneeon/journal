package bolt

import "gitlab.com/darleneeon/journal"

const passwordHashKey = "password-hash"

// Ensure SettingsService implements journal.SettingsService
var _ journal.SettingsService = &SettingsService{}

// SettingsService represents a service for managing settings.
type SettingsService struct {
	client *Client
}

// SetPasswordHash saves the password hash.
func (s *SettingsService) SetPasswordHash(hash []byte) error {
	// Start read-write transaction
	tx, err := s.client.db.Begin(true)
	if err != nil {
		return err
	}
	defer tx.Rollback()

	b := tx.Bucket([]byte(settingsBucket))

	if err := b.Put([]byte(passwordHashKey), hash); err != nil {
		return err
	}

	return tx.Commit()
}

// GetPasswordHash returns the password hash.
func (s *SettingsService) GetPasswordHash() ([]byte, error) {
	// Start read-only transaction
	tx, err := s.client.db.Begin(true)
	if err != nil {
		return nil, err
	}
	defer tx.Rollback()

	b := tx.Bucket([]byte(settingsBucket))

	return b.Get([]byte(passwordHashKey)), nil
}
