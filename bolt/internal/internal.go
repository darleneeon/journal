package internal

import (
	"time"

	proto "github.com/golang/protobuf/proto"
	"gitlab.com/darleneeon/journal"
)

//go:generate protoc --go_out=. internal.proto

// MarshalNote encodes a note to binary format.
func MarshalNote(n *journal.Note) ([]byte, error) {
	return proto.Marshal(&Note{
		ID:        string(n.ID),
		Text:      n.Text,
		Date:      n.Date.UnixNano(),
		Encrypted: n.Encrypted,
	})
}

// UnmarshalNote decodes a note from a binary data.
func UnmarshalNote(data []byte, n *journal.Note) error {
	var bn Note
	if err := proto.Unmarshal(data, &bn); err != nil {
		return err
	}

	n.ID = journal.NoteID(bn.ID)
	n.Text = bn.Text
	n.Date = time.Unix(0, bn.Date).UTC()
	n.Encrypted = bn.Encrypted

	return nil
}
