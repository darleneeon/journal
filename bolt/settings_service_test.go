package bolt_test

import (
	"reflect"
	"testing"
)

func TestSettingsService_OK(t *testing.T) {
	c := MustOpenClient()
	defer c.Close()
	s := c.SettingsService()

	hash := []byte("hash")
	if err := s.SetPasswordHash(hash); err != nil {
		t.Error(err)
	}

	if received, err := s.GetPasswordHash(); err != nil {
		t.Error(err)
	} else if !reflect.DeepEqual(received, hash) {
		t.Errorf(
			"Received wrong value. Expected [%s] but got [%s]\n",
			string(hash), string(received))
	}
}
