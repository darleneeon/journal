package bolt

import (
	"time"

	"github.com/boltdb/bolt"
	"gitlab.com/darleneeon/journal"
	"gitlab.com/darleneeon/journal/encryption"
)

const (
	notesBucket    = "Notes"
	settingsBucket = "Settings"
)

// Ensure Client implements journal.Client
var _ journal.Client = &Client{}

// Client represents a client to the underlying BoltDB data store.
type Client struct {
	// Filename to the BoltDB database.
	Path string

	// Returns the current time.
	Now func() time.Time

	// Services
	noteService       NoteService
	settingsService   SettingsService
	encryptionService encryption.EncryptionService

	db *bolt.DB
}

// NewClient returns a new instance of Client.
func NewClient() *Client {
	c := &Client{Now: time.Now}
	c.noteService.client = c
	c.settingsService.client = c
	return c
}

// Open opens and initializes the BoltDB database.
func (c *Client) Open() error {
	// Open database file.
	db, err := bolt.Open(c.Path, 0666, &bolt.Options{Timeout: 1 * time.Second})
	if err != nil {
		return err
	}
	c.db = db

	// Initialize top-level buckets.
	tx, err := c.db.Begin(true)
	if err != nil {
		return err
	}
	defer tx.Rollback()

	if _, err := tx.CreateBucketIfNotExists([]byte(notesBucket)); err != nil {
		return err
	}

	if _, err := tx.CreateBucketIfNotExists([]byte(settingsBucket)); err != nil {
		return err
	}

	return tx.Commit()
}

// Close closes then underlying BoltDB database.
func (c *Client) Close() error {
	if c.db != nil {
		return c.db.Close()
	}
	return nil
}

// NoteService returns the note service associated with the client.
func (c *Client) NoteService() journal.NoteService { return &c.noteService }

// SettingsService returns the settings service associated with the client.
func (c *Client) SettingsService() journal.SettingsService { return &c.settingsService }

// EncryptionService returns the encryption service associated with the client.
func (c *Client) EncryptionService() journal.EncryptionService { return &c.encryptionService }
