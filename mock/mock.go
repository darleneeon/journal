package mock

import "gitlab.com/darleneeon/journal"

// -----------------------------------------------------------------------------
// NoteService
// -----------------------------------------------------------------------------

// Ensure NoteService implements journal.NoteService
var _ journal.NoteService = &NoteService{}

type NoteService struct {
	NoteFn      func(id journal.NoteID) (*journal.Note, error)
	NoteInvoked bool

	NotesFn      func() ([]journal.Note, error)
	NotesInvoked bool

	SaveNoteFn      func(note *journal.Note) error
	SaveNoteInvoked bool

	DeleteNoteFn      func(id journal.NoteID) error
	DeleteNoteInvoked bool
}

func (s *NoteService) Note(id journal.NoteID) (*journal.Note, error) {
	s.NoteInvoked = true
	return s.NoteFn(id)
}

func (s *NoteService) Notes() ([]journal.Note, error) {
	s.NotesInvoked = true
	return s.NotesFn()
}

func (s *NoteService) SaveNote(Note *journal.Note) error {
	s.SaveNoteInvoked = true
	return s.SaveNoteFn(Note)
}

func (s *NoteService) DeleteNote(id journal.NoteID) error {
	s.DeleteNoteInvoked = true
	return s.DeleteNoteFn(id)
}

// -----------------------------------------------------------------------------
// EncryptionService
// -----------------------------------------------------------------------------

// Ensure EncryptionService implements journal.EncryptionService
var _ journal.EncryptionService = &EncryptionService{}

type EncryptionService struct {
	GeneratePasswordHashFn      func(password string) ([]byte, error)
	GeneratePasswordHashInvoked bool

	CompareHashAndPasswordFn      func(hash []byte, password string) error
	CompareHashAndPasswordInvoked bool

	EncryptNoteFn      func(note *journal.Note, password string) error
	EncryptNoteInvoked bool

	DecryptNoteFn      func(note *journal.Note, password string) error
	DecryptNoteInvoked bool
}

func (s *EncryptionService) GeneratePasswordHash(password string) ([]byte, error) {
	s.GeneratePasswordHashInvoked = true
	return s.GeneratePasswordHashFn(password)
}

func (s *EncryptionService) CompareHashAndPassword(hash []byte, password string) error {
	s.CompareHashAndPasswordInvoked = true
	return s.CompareHashAndPasswordFn(hash, password)
}

func (s *EncryptionService) EncryptNote(note *journal.Note, password string) error {
	s.EncryptNoteInvoked = true
	return s.EncryptNoteFn(note, password)
}

func (s *EncryptionService) DecryptNote(note *journal.Note, password string) error {
	s.DecryptNoteInvoked = true
	return s.DecryptNoteFn(note, password)
}

// -----------------------------------------------------------------------------
// SettingsService
// -----------------------------------------------------------------------------

// Ensure SettingsService implements journal.SettingsService
var _ journal.SettingsService = &SettingsService{}

type SettingsService struct {
	SetPasswordHashFn      func(hash []byte) error
	SetPasswordHashInvoked bool

	GetPasswordHashFn      func() ([]byte, error)
	GetPasswordHashInvoked bool
}

func (s *SettingsService) SetPasswordHash(hash []byte) error {
	s.SetPasswordHashInvoked = true
	return s.SetPasswordHashFn(hash)
}

func (s *SettingsService) GetPasswordHash() ([]byte, error) {
	s.GetPasswordHashInvoked = true
	return s.GetPasswordHashFn()
}
