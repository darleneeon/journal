package journal

import "time"

// NoteID represents note indentifier.
type NoteID string

const (
	// NoteIDFormat is default id format.
	NoteIDFormat = "02-01-06"

	// NoteDateFormat is default date format.
	NoteDateFormat = "January 2, 2006"
)

// Note represents a note.
type Note struct {
	ID        NoteID    `json:"id"`
	Text      string    `json:"text"`
	Date      time.Time `json:"date"`
	Encrypted bool      `json:"encrypted"`
}

// FormatDate returns note date in human-friendly format.
func (n *Note) FormatDate() string {
	return n.Date.Format(NoteDateFormat)
}

// Client creates a connection to the services.
type Client interface {
	NoteService() NoteService
	SettingsService() SettingsService
	EncryptionService() EncryptionService
}

// NoteService represents a service for managing notes.
type NoteService interface {
	Note(id NoteID) (*Note, error)
	Notes() ([]Note, error)
	SaveNote(note *Note) error
	DeleteNote(id NoteID) error
}

// SettingsService represents a service for managing settings.
type SettingsService interface {
	SetPasswordHash(hash []byte) error
	GetPasswordHash() ([]byte, error)
}

// EncryptionService represents a service for encrypting notes.
type EncryptionService interface {
	GeneratePasswordHash(password string) ([]byte, error)
	CompareHashAndPassword(hash []byte, password string) error
	EncryptNote(note *Note, password string) error
	DecryptNote(note *Note, password string) error
}
