package http

const (
	urlStatic        = "/static/"
	urlIndex         = "/"
	urlMenu          = "/menu"
	urlSettings      = "/settings"
	urlAuthorization = "/authorization"
	urlCreateNote    = "/create"
	urlSavedNotes    = "/saved"
	urlNote          = "/note/{id}/"
	urlDeleteNote    = "/note/{id}/delete"
)
