package http

import (
	"net/http"
	"net/url"
	"strings"
)

// SessionPasswordCheck is a middleware for checking session password.
func (h *Handler) SessionPasswordCheck(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// Allow requests that do not require the password
		if r.URL.Path == urlSettings || r.URL.Path == urlAuthorization ||
			strings.HasPrefix(r.URL.Path, urlStatic) {
			next.ServeHTTP(w, r)
			return
		}

		passwordHash, err := h.SettingsService.GetPasswordHash()
		if err != nil {
			sendError(w, err)
			return
		}

		// Redirect to the settings if password doesn't exist
		if len(passwordHash) == 0 {
			http.Redirect(w, r, urlSettings, http.StatusSeeOther)
			return
		}

		// Redirect to the authorization page if password is not set or incorrect
		if err := h.EncryptionService.
			CompareHashAndPassword(passwordHash, h.SessionPassword); err != nil {

			url, err := url.Parse(urlAuthorization)
			if err != nil {
				sendError(w, err)
				return
			}

			url.RawQuery = "next=" + r.RequestURI

			http.Redirect(w, r, url.String(), http.StatusSeeOther)
			return
		}

		// Otherwise, allow the request
		next.ServeHTTP(w, r)
	})
}
