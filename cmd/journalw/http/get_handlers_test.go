package http

import (
	"net/http"
	"strings"
	"testing"

	"gitlab.com/darleneeon/journal"
)

// -----------------------------------------------------------------------------
// MenuGET

func TestIndexGET(t *testing.T) {
	h := NewTestHandler()

	req, _ := http.NewRequest("GET", urlIndex, nil)
	res := response(req, h)

	checkResponseCode(http.StatusMovedPermanently, res, t)
	checkResponseRedirectLocation(urlMenu, res, t)
}

func TestMenuGET(t *testing.T) {
	h := NewTestHandler()

	req, _ := http.NewRequest("GET", urlMenu, nil)
	res := response(req, h)

	checkResponseCode(http.StatusOK, res, t)
	checkResponseBodyContains("Create note", res, t)
	checkResponseBodyContains("Saved notes", res, t)
	checkResponseBodyContains("Settings", res, t)
}

// -----------------------------------------------------------------------------
// SettingsGET

func TestSettingsGET_CreatePassword(t *testing.T) {
	h := NewTestHandler()

	// Set required mocks
	h.SettingsService.GetPasswordHashFn = func() ([]byte, error) {
		return nil, nil
	}

	req, _ := http.NewRequest("GET", urlSettings, nil)
	res := response(req, h)

	checkResponseCode(http.StatusOK, res, t)
	checkResponseBodyContains("New password", res, t)
	checkResponseBodyDoesNotContain("Password", res, t)
}

func TestSettingsGET_ChangePassword(t *testing.T) {
	h := NewTestHandler()

	// Set required mocks
	h.SettingsService.GetPasswordHashFn = func() ([]byte, error) {
		return []byte("password hash"), nil
	}

	req, _ := http.NewRequest("GET", urlSettings, nil)
	res := response(req, h)

	checkResponseCode(http.StatusOK, res, t)
	checkResponseBodyContains("Password", res, t)
	checkResponseBodyContains("New password", res, t)
}

// -----------------------------------------------------------------------------
// AuthorizationGET

func TestAuthorizationGET(t *testing.T) {
	h := NewTestHandler()

	req, _ := http.NewRequest("GET", urlAuthorization, nil)
	res := response(req, h)

	checkResponseCode(http.StatusOK, res, t)
	checkResponseBodyContains("Password", res, t)
}

// -----------------------------------------------------------------------------
// CreateNoteGET

func TestCreateNoteGET_NewNote(t *testing.T) {
	h := NewTestHandler()

	// Set required mocks
	h.NoteService.NoteFn = func(id journal.NoteID) (*journal.Note, error) {
		return nil, journal.ErrNoteNotFound
	}

	req, _ := http.NewRequest("GET", urlCreateNote, nil)
	res := response(req, h)

	checkResponseCode(http.StatusOK, res, t)
}

func TestCreateNoteGET_EditNote(t *testing.T) {
	h := NewTestHandler()

	// Set required mocks
	h.NoteService.NoteFn = func(id journal.NoteID) (*journal.Note, error) {
		return &journal.Note{Text: "note text"}, nil
	}
	h.EncryptionService.DecryptNoteFn = func(note *journal.Note, password string) error {
		return nil
	}

	req, _ := http.NewRequest("GET", urlCreateNote, nil)
	res := response(req, h)

	checkResponseCode(http.StatusOK, res, t)
	checkResponseBodyContains("note text", res, t)
}

// -----------------------------------------------------------------------------
// SavedNotesGET

func TestSavedNotesGET(t *testing.T) {
	h := NewTestHandler()

	// Set required mocks
	h.NoteService.NotesFn = func() ([]journal.Note, error) {
		note1 := journal.Note{Date: Now}
		note2 := journal.Note{Date: Now.AddDate(0, 0, 1)}

		return []journal.Note{note1, note2}, nil
	}

	req, _ := http.NewRequest("GET", urlSavedNotes, nil)
	res := response(req, h)

	checkResponseCode(http.StatusOK, res, t)
	checkResponseBodyContains(Now.Format(journal.NoteDateFormat), res, t)
	checkResponseBodyContains(Now.AddDate(0, 0, 1).Format(journal.NoteDateFormat), res, t)
}

func TestSavedNotesGET_NoNotes(t *testing.T) {
	h := NewTestHandler()

	// Set required mocks
	h.NoteService.NotesFn = func() ([]journal.Note, error) {
		return nil, nil
	}

	req, _ := http.NewRequest("GET", urlSavedNotes, nil)
	res := response(req, h)

	checkResponseCode(http.StatusOK, res, t)
	checkResponseBodyContains("No saved notes", res, t)
}

// -----------------------------------------------------------------------------
// NoteGET

func TestNoteGET_OK(t *testing.T) {
	h := NewTestHandler()

	// Set required mocks
	h.NoteService.NoteFn = func(id journal.NoteID) (*journal.Note, error) {
		if id == "EXISTING" {
			return &journal.Note{Text: "note text"}, nil
		}

		return nil, journal.ErrNoteNotFound
	}
	h.EncryptionService.DecryptNoteFn = func(note *journal.Note, password string) error {
		return nil
	}

	req, _ := http.NewRequest("GET", strings.Replace(urlNote, "{id}", "EXISTING", 1), nil)
	res := response(req, h)

	checkResponseCode(http.StatusOK, res, t)
	checkResponseBodyContains("note text", res, t)
}

func TestNoteGET_ErrNoteNotFound(t *testing.T) {
	h := NewTestHandler()

	// Set required mocks
	h.NoteService.NoteFn = func(id journal.NoteID) (*journal.Note, error) {
		return nil, journal.ErrNoteNotFound
	}

	req, _ := http.NewRequest("GET", strings.Replace(urlNote, "{id}", "NONEXISTENT", 1), nil)
	res := response(req, h)

	checkResponseCode(http.StatusNotFound, res, t)
}
