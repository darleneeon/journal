package http

import (
	"net/http"
	"strings"
	"time"

	"github.com/go-chi/chi"
	"gitlab.com/darleneeon/journal"
)

// -----------------------------------------------------------------------------
// MenuGET

func (h *Handler) handleIndexGET(w http.ResponseWriter, r *http.Request) {
	http.Redirect(w, r, urlMenu, http.StatusMovedPermanently)
}

func (h *Handler) handleMenuGET(w http.ResponseWriter, r *http.Request) {
	p := MenuPage{
		CreateNoteURL: urlCreateNote,
		SavedNotesURL: urlSavedNotes,
		SettingsURL:   urlSettings,
	}
	p.Title = "Menu - jouranlw"
	p.MenuURL = urlMenu

	if err := renderHTML(w, p, "base.html", "menu.html"); err != nil {
		sendError(w, err)
	}
}

// -----------------------------------------------------------------------------
// SettingsGET

func (h *Handler) handleSettingsGET(w http.ResponseWriter, r *http.Request) {
	p := SettingsPage{
		SettingsURL: urlSettings,
	}
	p.Title = "Settings - journalw"
	p.MenuURL = urlMenu

	passwordHash, err := h.SettingsService.GetPasswordHash()
	if err != nil {
		sendError(w, err)
		return
	}

	p.Form.Change = len(passwordHash) > 0

	if err := renderHTML(w, p, "base.html", "settings.html"); err != nil {
		sendError(w, err)
	}
}

// -----------------------------------------------------------------------------
// AuthorizationGET

func (h *Handler) handleAuthorizationGET(w http.ResponseWriter, r *http.Request) {
	p := AuthorizationPage{
		AuthorizationURL: r.RequestURI,
	}
	p.Title = "Authorization - journalw"
	p.MenuURL = urlMenu

	if err := renderHTML(w, p, "base.html", "authorization.html"); err != nil {
		sendError(w, err)
	}
}

// -----------------------------------------------------------------------------
// CreateNoteGET

func (h *Handler) handleCreateNoteGET(w http.ResponseWriter, r *http.Request) {
	p := CreateNotePage{
		CreateNoteURL: urlCreateNote,
	}
	p.Title = "Create note - journalw"
	p.MenuURL = urlMenu

	todayNoteID := journal.NoteID(time.Now().Format(journal.NoteIDFormat))
	todayNote, err := h.NoteService.Note(todayNoteID)
	if err == nil {
		if err := h.EncryptionService.
			DecryptNote(todayNote, h.SessionPassword); err != nil {
			sendError(w, err)
			return
		}
		p.Form.Text = todayNote.Text
	} else if err != nil && err != journal.ErrNoteNotFound {
		sendError(w, err)
		return
	}

	if err := renderHTML(w, p, "base.html", "create.html"); err != nil {
		sendError(w, err)
	}
}

// -----------------------------------------------------------------------------
// SavedNotesGET

func (h *Handler) handleSavedNotesGET(w http.ResponseWriter, r *http.Request) {
	notes, err := h.NoteService.Notes()
	if err != nil {
		sendError(w, err)
		return
	}

	p := SavedNotesPage{
		Notes: notes,
	}
	p.Title = "Saved notes - journalw"
	p.MenuURL = urlMenu

	if err := renderHTML(w, p, "base.html", "saved.html"); err != nil {
		sendError(w, err)
	}
}

// -----------------------------------------------------------------------------
// NoteGET

func (h *Handler) handleNoteGET(w http.ResponseWriter, r *http.Request) {
	note, err := h.NoteService.Note(journal.NoteID(chi.URLParam(r, "id")))
	if err == journal.ErrNoteNotFound {
		http.NotFound(w, r)
		return
	} else if err != nil {
		sendError(w, err)
		return
	}

	if err := h.EncryptionService.DecryptNote(note, h.SessionPassword); err != nil {
		sendError(w, err)
		return
	}

	p := NotePage{
		NoteDate:      note.FormatDate(),
		Paragraphs:    strings.Split(note.Text, "\n"),
		EditAllowed:   note.Date.Format(journal.NoteDateFormat) == time.Now().Format(journal.NoteDateFormat),
		EditNoteURL:   urlCreateNote,
		DeleteNoteURL: strings.Replace(urlDeleteNote, "{id}", string(note.ID), 1),
	}
	p.Title = note.FormatDate() + " - journalw"
	p.MenuURL = urlMenu

	if err := renderHTML(w, p, "base.html", "note.html"); err != nil {
		sendError(w, err)
	}
}
