package http

import (
	"net/http"
	"net/url"
	"strings"
	"testing"
	"time"

	"gitlab.com/darleneeon/journal"
)

// -----------------------------------------------------------------------------
// SettignsPOST

func TestSettingsPOST_CreatePassword_OK(t *testing.T) {
	h := NewTestHandler()

	// Set required mocks
	h.SettingsService.GetPasswordHashFn = func() ([]byte, error) {
		return nil, nil
	}
	h.SettingsService.SetPasswordHashFn = func(hash []byte) error {
		return nil
	}
	h.EncryptionService.GeneratePasswordHashFn = func(password string) ([]byte, error) {
		return []byte("password hash"), nil
	}

	form := url.Values{}
	form.Add("new-password", "qwerty123")

	req, _ := http.NewRequest("POST", urlSettings, nil)
	req.PostForm = form
	res := response(req, h)

	checkResponseCode(http.StatusSeeOther, res, t)
	checkResponseRedirectLocation(urlMenu, res, t)
}

func TestSettingsPOST_CreatePassword_ErrPasswordRequired(t *testing.T) {
	h := NewTestHandler()

	// Set required mocks
	h.SettingsService.GetPasswordHashFn = func() ([]byte, error) {
		return nil, nil
	}

	form := url.Values{}
	form.Add("new-password", "")

	req, _ := http.NewRequest("POST", urlSettings, nil)
	req.PostForm = form
	res := response(req, h)

	checkResponseCode(http.StatusOK, res, t)
	checkResponseBodyContains(journal.ErrPasswordRequired.Error(), res, t)
}

func TestSettingsPOST_ChangePassword_OK(t *testing.T) {
	h := NewTestHandler()

	// Set required mocks
	h.SettingsService.GetPasswordHashFn = func() ([]byte, error) {
		return []byte("password hash"), nil
	}
	h.SettingsService.SetPasswordHashFn = func(hash []byte) error {
		return nil
	}
	h.EncryptionService.CompareHashAndPasswordFn = func(hash []byte, password string) error {
		return nil
	}
	h.EncryptionService.GeneratePasswordHashFn = func(password string) ([]byte, error) {
		return []byte("new password hash"), nil
	}
	h.EncryptionService.DecryptNoteFn = func(note *journal.Note, password string) error {
		return nil
	}
	h.EncryptionService.EncryptNoteFn = func(note *journal.Note, password string) error {
		return nil
	}
	h.NoteService.NotesFn = func() ([]journal.Note, error) {
		note1 := journal.Note{Text: "encrypted", Encrypted: true}
		note2 := journal.Note{Text: "plain", Encrypted: false}
		return []journal.Note{note1, note2}, nil
	}
	h.NoteService.SaveNoteFn = func(note *journal.Note) error {
		return nil
	}

	form := url.Values{}
	form.Add("password", "qwerty123")
	form.Add("new-password", "qwerty321")

	req, _ := http.NewRequest("POST", urlSettings, nil)
	req.PostForm = form
	res := response(req, h)

	checkResponseCode(http.StatusSeeOther, res, t)
	checkResponseRedirectLocation(urlMenu, res, t)
}

func TestSettingsPOST_ChangePassword_ErrEqualPasswords(t *testing.T) {
	h := NewTestHandler()

	// Set required mocks
	h.SettingsService.GetPasswordHashFn = func() ([]byte, error) {
		return []byte("password hash"), nil
	}

	form := url.Values{}
	form.Add("password", "qwerty123")
	form.Add("new-password", "qwerty123")

	req, _ := http.NewRequest("POST", urlSettings, nil)
	req.PostForm = form
	res := response(req, h)

	checkResponseCode(http.StatusOK, res, t)
	checkResponseBodyContains(journal.ErrEqualPasswords.Error(), res, t)
}

func TestSettingsPOST_ChangePassword_ErrPasswordsRequired(t *testing.T) {
	h := NewTestHandler()

	// Set required mocks
	h.SettingsService.GetPasswordHashFn = func() ([]byte, error) {
		return []byte("password hash"), nil
	}

	form := url.Values{}
	form.Add("password", "")
	form.Add("new-password", "")

	req, _ := http.NewRequest("POST", urlSettings, nil)
	req.PostForm = form
	res := response(req, h)

	checkResponseCode(http.StatusOK, res, t)
	checkResponseBodyContains(journal.ErrPasswordRequired.Error(), res, t)
	checkResponseBodyContains(journal.ErrNewPasswordRequired.Error(), res, t)
}

func TestSettingsPOST_ChangePassword_ErrPasswordRequired(t *testing.T) {
	h := NewTestHandler()

	// Set required mocks
	h.SettingsService.GetPasswordHashFn = func() ([]byte, error) {
		return []byte("password hash"), nil
	}

	form := url.Values{}
	form.Add("password", "")
	form.Add("new-password", "qwerty321")

	req, _ := http.NewRequest("POST", urlSettings, nil)
	req.PostForm = form
	res := response(req, h)

	checkResponseCode(http.StatusOK, res, t)
	checkResponseBodyContains(journal.ErrPasswordRequired.Error(), res, t)
}

func TestSettingsPOST_ChangePassword_ErrNewPasswordRequired(t *testing.T) {
	h := NewTestHandler()

	// Set required mocks
	h.SettingsService.GetPasswordHashFn = func() ([]byte, error) {
		return []byte("password hash"), nil
	}

	form := url.Values{}
	form.Add("password", "qwerty123")
	form.Add("new-password", "")

	req, _ := http.NewRequest("POST", urlSettings, nil)
	req.PostForm = form
	res := response(req, h)

	checkResponseCode(http.StatusOK, res, t)
	checkResponseBodyContains(journal.ErrNewPasswordRequired.Error(), res, t)
}

func TestSettingsPOST_ChangePassword_ErrWrongPassword(t *testing.T) {
	h := NewTestHandler()

	// Set required mocks
	h.SettingsService.GetPasswordHashFn = func() ([]byte, error) {
		return []byte("password hash"), nil
	}
	h.EncryptionService.CompareHashAndPasswordFn = func(hash []byte, password string) error {
		return journal.ErrWrongPassword
	}

	form := url.Values{}
	form.Add("password", "qwerty123")
	form.Add("new-password", "qwerty321")

	req, _ := http.NewRequest("POST", urlSettings, nil)
	req.PostForm = form
	res := response(req, h)

	checkResponseCode(http.StatusOK, res, t)
	checkResponseBodyContains(journal.ErrWrongPassword.Error(), res, t)
}

// -----------------------------------------------------------------------------
// AuthorizationPOST

func TestAuthorizationPOST_OK(t *testing.T) {
	h := NewTestHandler()

	// Set required mocks
	h.SettingsService.GetPasswordHashFn = func() ([]byte, error) {
		return []byte("password hash"), nil
	}
	h.EncryptionService.CompareHashAndPasswordFn = func(hash []byte, password string) error {
		return nil
	}

	form := url.Values{}
	form.Add("password", "qwerty123")

	req, _ := http.NewRequest("POST", urlAuthorization, nil)
	req.PostForm = form
	res := response(req, h)

	checkResponseCode(http.StatusSeeOther, res, t)
	checkResponseRedirectLocation(urlMenu, res, t)

	if len(h.Handler.SessionPassword) == 0 {
		t.Error("Session password was not set")
	} else if h.Handler.SessionPassword != "qwerty123" {
		t.Errorf(
			"Session password is not correct: expected %s but got %s\n",
			"qwerty123", h.Handler.SessionPassword)
	}
}

func TestAuthorizationPOST_ErrPasswordRequired(t *testing.T) {
	h := NewTestHandler()

	// Set required mocks
	h.SettingsService.GetPasswordHashFn = func() ([]byte, error) {
		return []byte("password hash"), nil
	}

	form := url.Values{}
	form.Add("password", "")

	req, _ := http.NewRequest("POST", urlAuthorization, nil)
	req.PostForm = form
	res := response(req, h)

	checkResponseCode(http.StatusOK, res, t)
	checkResponseBodyContains(journal.ErrPasswordRequired.Error(), res, t)
}

func TestAuthorizationPOST_ErrWrongPassword(t *testing.T) {
	h := NewTestHandler()

	// Set required mocks
	h.SettingsService.GetPasswordHashFn = func() ([]byte, error) {
		return []byte("password hash"), nil
	}
	h.EncryptionService.CompareHashAndPasswordFn = func(hash []byte, password string) error {
		return journal.ErrWrongPassword
	}

	form := url.Values{}
	form.Add("password", "qwerty123")

	req, _ := http.NewRequest("POST", urlAuthorization, nil)
	req.PostForm = form
	res := response(req, h)

	checkResponseCode(http.StatusOK, res, t)
	checkResponseBodyContains(journal.ErrWrongPassword.Error(), res, t)
}

// -----------------------------------------------------------------------------
// CreateNotePOST

func TestCreateNotePOST_OK(t *testing.T) {
	h := NewTestHandler()

	// Set required mocks
	h.SettingsService.GetPasswordHashFn = func() ([]byte, error) {
		return nil, nil
	}
	h.EncryptionService.CompareHashAndPasswordFn = func(hash []byte, password string) error {
		return nil
	}
	h.EncryptionService.EncryptNoteFn = func(note *journal.Note, password string) error {
		return nil
	}
	h.NoteService.SaveNoteFn = func(note *journal.Note) error {
		if note.ID != journal.NoteID(time.Now().Format(journal.NoteIDFormat)) {
			return journal.ErrInternal
		} else if len(note.Text) == 0 {
			return journal.ErrNoteTextRequired
		}

		return nil
	}

	form := url.Values{}
	form.Add("text", "note text")

	req, _ := http.NewRequest("POST", urlCreateNote, nil)
	req.PostForm = form
	res := response(req, h)

	checkResponseCode(http.StatusSeeOther, res, t)
}

func TestCreateNotePOST_ErrNoteTextRequired(t *testing.T) {
	h := NewTestHandler()

	// Set required mocks
	h.SettingsService.GetPasswordHashFn = func() ([]byte, error) {
		return nil, nil
	}
	h.EncryptionService.CompareHashAndPasswordFn = func(hash []byte, password string) error {
		return nil
	}

	form := url.Values{}
	form.Add("text", "")

	req, _ := http.NewRequest("POST", urlCreateNote, nil)
	req.PostForm = form
	res := response(req, h)

	checkResponseCode(http.StatusOK, res, t)
	checkResponseBodyContains(journal.ErrNoteTextRequired.Error(), res, t)
}

func TestCreateNotePOST_ErrInternal(t *testing.T) {
	h := NewTestHandler()

	// Set required mocks
	h.SettingsService.GetPasswordHashFn = func() ([]byte, error) {
		return nil, nil
	}
	h.EncryptionService.CompareHashAndPasswordFn = func(hash []byte, password string) error {
		return journal.ErrInternal
	}

	form := url.Values{}
	form.Add("text", "note text")

	req, _ := http.NewRequest("POST", urlCreateNote, nil)
	req.PostForm = form
	res := response(req, h)

	checkResponseCode(http.StatusInternalServerError, res, t)
}

// -----------------------------------------------------------------------------
// DeleteNotePOST

func TestDeleteNotePOST_OK(t *testing.T) {
	h := NewTestHandler()

	// Set required mocks
	h.NoteService.DeleteNoteFn = func(id journal.NoteID) error {
		if id == "EXISTING" {
			return nil
		}

		return journal.ErrNoteNotFound
	}

	req, _ := http.NewRequest("POST", strings.Replace(urlDeleteNote, "{id}", "EXISTING", 1), nil)
	res := response(req, h)

	checkResponseCode(http.StatusSeeOther, res, t)
	checkResponseRedirectLocation(urlMenu, res, t)
}

func TestDeleteNotePOST_ErrNoteNotFound(t *testing.T) {
	h := NewTestHandler()

	// Set required mocks
	h.NoteService.DeleteNoteFn = func(id journal.NoteID) error {
		return journal.ErrNoteNotFound
	}

	req, _ := http.NewRequest("POST", strings.Replace(urlDeleteNote, "{id}", "NONEXISTENT", 1), nil)
	res := response(req, h)

	checkResponseCode(http.StatusNotFound, res, t)
}
