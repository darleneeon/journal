package http

import (
	"log"
	"net"
	"net/http"
)

// DefaultAddr is the default bind adress.
const DefaultAddr = ":8080"

// Server represents the HTTP server.
type Server struct {
	ln net.Listener

	Addr string

	Handler *Handler

	Log *log.Logger
}

// NewServer returns a new instance of App.
func NewServer() *Server {
	s := &Server{
		Addr: DefaultAddr,
	}

	return s
}

// Start opens a socket and serves HTTP server.
func (s *Server) Start() error {
	// Open a socket.
	ln, err := net.Listen("tcp", s.Addr)
	if err != nil {
		return err
	}
	s.ln = ln

	// Start HTTP server.
	go func() { http.Serve(s.ln, s.Handler) }()

	return nil
}

// Stop closes the socket.
func (s *Server) Stop() error {
	if s.ln != nil {
		return s.ln.Close()
	}

	return nil
}
