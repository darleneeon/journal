package http

import (
	"log"
	"os"
)

// App represents the HTTP application.
type App struct {
	*Server

	Handler *Handler

	Log *log.Logger
}

// NewApp returns a new instance of App.
func NewApp(middleware bool) *App {
	a := &App{
		Handler: NewHandler(middleware),
		Server:  NewServer(),
		Log:     log.New(os.Stdout, "", log.LstdFlags),
	}

	a.Handler.Log = a.Log

	a.Server.Handler = a.Handler
	a.Server.Log = a.Log

	return a
}
