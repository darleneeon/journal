package http

import (
	"log"
	"net/http"

	"github.com/go-chi/chi"
	"gitlab.com/darleneeon/journal"
	"gitlab.com/darleneeon/journal/cmd/journalw/assets"
)

// Handler represents the main HTTP handler.
type Handler struct {
	*chi.Mux

	SessionPassword string

	NoteService       journal.NoteService
	SettingsService   journal.SettingsService
	EncryptionService journal.EncryptionService

	Log *log.Logger
}

// NewHandler returns a new instance of Handler.
func NewHandler(middleware bool) *Handler {
	h := &Handler{
		Mux: chi.NewRouter(),
	}

	// Set middleware
	if middleware {
		h.Use(h.SessionPasswordCheck)
	}

	// Static
	staticDir, err := assets.StaticPath()
	if err != nil {
		panic(err)
	}

	h.Get("/static/*", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		http.StripPrefix("/static/", http.FileServer(http.Dir(staticDir))).ServeHTTP(w, r)
	}))

	// Get handlers
	h.Get(urlIndex, h.handleIndexGET)
	h.Get(urlMenu, h.handleMenuGET)
	h.Get(urlSettings, h.handleSettingsGET)
	h.Get(urlAuthorization, h.handleAuthorizationGET)
	h.Get(urlCreateNote, h.handleCreateNoteGET)
	h.Get(urlSavedNotes, h.handleSavedNotesGET)
	h.Get(urlNote, h.handleNoteGET)

	// Post handlers
	h.Post(urlSettings, h.handleSettingsPOST)
	h.Post(urlAuthorization, h.handleAuthorizationPOST)
	h.Post(urlCreateNote, h.handleCreateNotePOST)
	h.Post(urlDeleteNote, h.handleDeleteNotePOST)

	return h
}

// sendError sends error to the client.
func sendError(w http.ResponseWriter, err error) {
	http.Error(w, err.Error(), http.StatusInternalServerError)
}
