package http

import (
	"net/http"
	"net/url"
	"testing"

	"gitlab.com/darleneeon/journal"
)

func TestSessionPasswordCheck_unprotectedURIS(t *testing.T) {
	h := NewTestHandler()

	// required for SettingsGET
	h.SettingsService.GetPasswordHashFn = func() ([]byte, error) {
		return nil, nil
	}

	m := h.SessionPasswordCheck(h)

	unprotectedURIS := []string{urlAuthorization, urlSettings}
	for _, uri := range unprotectedURIS {
		req, _ := http.NewRequest("GET", uri, nil)
		req.RequestURI = uri
		res := response(req, m)

		if res.HeaderMap.Get("Location") == urlAuthorization {
			t.Errorf("Handler asks for authorizaton for URI: %s\n", uri)
		}
	}
}

func TestSessionPasswordCheck_OK(t *testing.T) {
	h := NewTestHandler()

	// set password
	h.SessionPassword = "qwerty123"

	// required for middleware
	h.SettingsService.GetPasswordHashFn = func() ([]byte, error) {
		return []byte("password hash"), nil
	}
	h.EncryptionService.CompareHashAndPasswordFn = func(hash []byte, password string) error {
		return nil
	}

	// required for CreateNoteGET and NoteGET
	h.NoteService.NoteFn = func(id journal.NoteID) (*journal.Note, error) {
		return &journal.Note{Text: "note text", Date: Now}, nil
	}
	h.EncryptionService.DecryptNoteFn = func(note *journal.Note, password string) error {
		return nil
	}

	// required for SavedNotesGET
	h.NoteService.NotesFn = func() ([]journal.Note, error) {
		return nil, nil
	}

	m := h.SessionPasswordCheck(h)

	protectedURIS := []string{urlCreateNote, urlSavedNotes, urlNote}
	for _, uri := range protectedURIS {
		req, _ := http.NewRequest("GET", uri, nil)
		req.RequestURI = uri
		res := response(req, m)

		checkResponseCode(http.StatusOK, res, t)
	}

}
func TestSessionPasswordCheck_NoPassword(t *testing.T) {
	h := NewTestHandler()

	// required for middleware
	h.SettingsService.GetPasswordHashFn = func() ([]byte, error) {
		return nil, nil
	}

	m := h.SessionPasswordCheck(h)

	protectedURIS := []string{urlCreateNote, urlSavedNotes, urlNote}
	for _, uri := range protectedURIS {
		req, _ := http.NewRequest("GET", uri, nil)
		req.RequestURI = uri
		res := response(req, m)

		checkResponseCode(http.StatusSeeOther, res, t)
		checkResponseRedirectLocation(urlSettings, res, t)
	}
}
func TestSessionPasswordCheck_WrongPassword(t *testing.T) {
	h := NewTestHandler()

	// set password
	h.SessionPassword = "wrongpassword"

	// required for middleware
	h.SettingsService.GetPasswordHashFn = func() ([]byte, error) {
		return []byte("password hash"), nil
	}
	h.EncryptionService.CompareHashAndPasswordFn = func(hash []byte, password string) error {
		return journal.ErrWrongPassword
	}

	m := h.SessionPasswordCheck(h)

	protectedURIS := []string{urlCreateNote, urlSavedNotes, urlNote}
	for _, uri := range protectedURIS {
		req, _ := http.NewRequest("GET", uri, nil)
		req.RequestURI = uri
		res := response(req, m)

		expectedRedirectURL, err := url.Parse(urlAuthorization)
		if err != nil {
			panic(err)
		}

		expectedRedirectURL.RawQuery = "next=" + uri

		checkResponseCode(http.StatusSeeOther, res, t)
		checkResponseRedirectLocation(expectedRedirectURL.String(), res, t)
	}
}
