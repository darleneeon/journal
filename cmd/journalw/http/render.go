package http

import (
	"fmt"
	"html/template"
	"net/http"
	"os"
	"path/filepath"

	"gitlab.com/darleneeon/journal/cmd/journalw/assets"
)

// renderHTML renders templates and sends html to the response.
func renderHTML(w http.ResponseWriter, data interface{}, tmpls ...string) error {
	templatesPath, err := assets.TemplatesPath()
	if err != nil {
		return err
	}

	// Generate full path for each template and check it.
	for i, tmpl := range tmpls {
		tmpl := filepath.FromSlash(fmt.Sprintf("%s/%s", templatesPath, tmpl))
		if _, err := os.Stat(tmpl); os.IsNotExist(err) {
			return err
		}

		tmpls[i] = tmpl
	}

	// Parse templates into single instance.
	tmpl, err := template.ParseFiles(tmpls...)
	if err != nil {
		return err
	}

	return tmpl.Execute(w, data)
}
