package http

import (
	"io/ioutil"
	"net/http"
	"time"

	"github.com/go-chi/chi"
	"gitlab.com/darleneeon/journal"
)

// -----------------------------------------------------------------------------
// SettignsPOST

func (h *Handler) handleSettingsPOST(w http.ResponseWriter, r *http.Request) {
	passwordHash, err := h.SettingsService.GetPasswordHash()
	if err != nil {
		sendError(w, err)
		return
	}

	passwordForm := journal.PasswordForm{
		Change:      len(passwordHash) > 0,
		Password:    r.FormValue("password"),
		NewPassword: r.FormValue("new-password"),
	}

	// Render form with errors if validation fails
	if err := passwordForm.Validate(); err != nil {
		p := SettingsPage{
			SettingsURL: urlSettings,
		}
		p.Title = "Settings - journalw"
		p.MenuURL = urlMenu
		p.Form = passwordForm

		if err := renderHTML(w, p, "base.html", "settings.html"); err != nil {
			sendError(w, err)
		}
		return
	}

	if passwordForm.Change {
		// Change the password

		// Check if password is correct
		err := h.EncryptionService.CompareHashAndPassword(passwordHash, passwordForm.Password)
		if err == journal.ErrWrongPassword {
			p := SettingsPage{
				SettingsURL: urlSettings,
			}
			p.Title = "Settings - journalw"
			p.MenuURL = urlMenu
			p.Form = passwordForm
			p.Form.PasswordErr = journal.ErrWrongPassword.Error()

			if err := renderHTML(w, p, "base.html", "settings.html"); err != nil {
				sendError(w, err)
			}
			return
		} else if err != nil {
			sendError(w, err)
			return
		}

		notes, err := h.NoteService.Notes()
		if err != nil {
			sendError(w, err)
			return
		}

		for _, note := range notes {
			if err := h.EncryptionService.DecryptNote(&note, passwordForm.Password); err != nil {
				h.Log.Printf(
					"Error while decrypting note [%s]: %s\n", note.FormatDate(), err.Error())
				continue
			} else if err := h.EncryptionService.EncryptNote(&note, passwordForm.NewPassword); err != nil {
				h.Log.Printf(
					"Error while encrypting note [%s]: %s\n", note.FormatDate(), err.Error())
				continue
			} else if err := h.NoteService.SaveNote(&note); err != nil {
				h.Log.Printf(
					"Error while saving note [%s]: %s\n", note.FormatDate(), err.Error())
			}
		}

		newPasswordHash, err := h.EncryptionService.GeneratePasswordHash(passwordForm.NewPassword)
		if err != nil {
			sendError(w, err)
			return
		}

		if err := h.SettingsService.SetPasswordHash(newPasswordHash); err != nil {
			sendError(w, err)
			return
		}

		// Save new password in the temporary file
		f, err := ioutil.TempFile("", "npswd")
		if err != nil {
			h.Log.Println(err)
		}

		_, err = f.WriteString(passwordForm.NewPassword)
		if err != nil {
			h.Log.Println(err)
		}
	} else {
		// Create new password

		passwordHash, err = h.EncryptionService.GeneratePasswordHash(passwordForm.NewPassword)
		if err != nil {
			sendError(w, err)
			return
		}

		if err := h.SettingsService.SetPasswordHash(passwordHash); err != nil {
			sendError(w, err)
			return
		}
	}

	http.Redirect(w, r, urlMenu, http.StatusSeeOther)
}

// -----------------------------------------------------------------------------
// AuthorizationPOST

func (h *Handler) handleAuthorizationPOST(w http.ResponseWriter, r *http.Request) {
	passwordHash, err := h.SettingsService.GetPasswordHash()
	if err != nil {
		sendError(w, err)
	}

	authorizationForm := journal.AuthorizationForm{
		Password: r.FormValue("password"),
	}

	if err := authorizationForm.Validate(); err != nil {
		p := AuthorizationPage{
			AuthorizationURL: r.RequestURI,
		}
		p.Title = "Authorization - journalw"
		p.MenuURL = urlMenu
		p.Form = authorizationForm

		if err := renderHTML(w, p, "base.html", "authorization.html"); err != nil {
			sendError(w, err)
		}
		return
	}

	err = h.EncryptionService.
		CompareHashAndPassword(passwordHash, authorizationForm.Password)
	if err == journal.ErrWrongPassword {
		p := AuthorizationPage{
			AuthorizationURL: r.RequestURI,
		}
		p.Title = "Authorization - journalw"
		p.MenuURL = urlMenu
		p.Form = authorizationForm
		p.Form.PasswordErr = err.Error()

		if err := renderHTML(w, p, "base.html", "authorization.html"); err != nil {
			sendError(w, err)
		}
		return
	} else if err != nil {
		sendError(w, err)
		return
	}

	h.SessionPassword = authorizationForm.Password

	if r.FormValue("next") == "" {
		http.Redirect(w, r, urlMenu, http.StatusSeeOther)
	} else {
		http.Redirect(w, r, r.FormValue("next"), http.StatusSeeOther)
	}
}

// -----------------------------------------------------------------------------
// CreateNotePOST

func (h *Handler) handleCreateNotePOST(w http.ResponseWriter, r *http.Request) {
	passwordHash, err := h.SettingsService.GetPasswordHash()
	if err != nil {
		sendError(w, err)
		return
	}

	if err := h.EncryptionService.
		CompareHashAndPassword(passwordHash, h.SessionPassword); err != nil {
		sendError(w, err)
		return
	}

	noteForm := journal.NoteForm{
		Text: r.FormValue("text"),
	}

	if err := noteForm.Validate(); err != nil {
		p := CreateNotePage{
			CreateNoteURL: urlCreateNote,
		}
		p.Title = "Create note - journalw"
		p.MenuURL = urlMenu
		p.Form = noteForm

		if err := renderHTML(w, p, "base.html", "create.html"); err != nil {
			sendError(w, err)
		}
		return
	}

	note := &journal.Note{
		ID:   journal.NoteID(time.Now().Format(journal.NoteIDFormat)),
		Text: noteForm.Text,
	}

	if err := h.EncryptionService.EncryptNote(note, h.SessionPassword); err != nil {
		sendError(w, err)
		return
	}

	if err := h.NoteService.SaveNote(note); err != nil {
		sendError(w, err)
		return
	}

	http.Redirect(w, r, urlMenu, http.StatusSeeOther)
}

// -----------------------------------------------------------------------------
// DeleteNotePOST

func (h *Handler) handleDeleteNotePOST(w http.ResponseWriter, r *http.Request) {
	err := h.NoteService.DeleteNote(journal.NoteID(chi.URLParam(r, "id")))
	if err == journal.ErrNoteNotFound {
		http.NotFound(w, r)
		return
	} else if err != nil {
		sendError(w, err)
		return
	}

	http.Redirect(w, r, urlMenu, http.StatusSeeOther)
}
