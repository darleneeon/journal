package http

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
	"time"

	"gitlab.com/darleneeon/journal/mock"
)

// Now is the mocked current time for testing.
var Now = time.Date(2000, time.January, 1, 0, 0, 0, 0, time.UTC)

// TestHandler is a test wrapper of Handler.
type TestHandler struct {
	*Handler

	NoteService       mock.NoteService
	SettingsService   mock.SettingsService
	EncryptionService mock.EncryptionService
}

// NewTestHandler returns a new instance of TestHandler.
func NewTestHandler() *TestHandler {
	h := &TestHandler{
		Handler: NewHandler(false),
	}

	h.Handler.NoteService = &h.NoteService
	h.Handler.SettingsService = &h.SettingsService
	h.Handler.EncryptionService = &h.EncryptionService

	return h
}

// -----------------------------------------------------------------------------
// Helpers

func response(r *http.Request, h http.Handler) *httptest.ResponseRecorder {
	rec := httptest.NewRecorder()
	h.ServeHTTP(rec, r)

	return rec
}

func checkResponseCode(expected int, res *httptest.ResponseRecorder, t *testing.T) {
	if expected != res.Code {
		t.Errorf("Wrong response code: expected %d but got %d\n", expected, res.Code)
	}
}

func checkResponseRedirectLocation(expected string, res *httptest.ResponseRecorder, t *testing.T) {
	if res.HeaderMap.Get("Location") != expected {
		t.Errorf(
			"Wrong response redirect location: expected [%s] but got [%s]\n",
			expected, res.HeaderMap.Get("Location"))
	}
}

func checkResponseBodyContains(substr string, res *httptest.ResponseRecorder, t *testing.T) {
	bodyString := fmt.Sprint(res.Body)
	if !strings.Contains(bodyString, substr) {
		t.Errorf(
			"Body doesn't contain [%s]\nBody:\n%s\n",
			substr, bodyString)
	}
}

func checkResponseBodyDoesNotContain(substr string, res *httptest.ResponseRecorder, t *testing.T) {
	bodyString := fmt.Sprint(res.Body)
	if strings.Contains(bodyString, substr) {
		t.Errorf(
			"Body contains [%s], but it shouldn't\nBody:\n%s\n",
			substr, bodyString)
	}
}
