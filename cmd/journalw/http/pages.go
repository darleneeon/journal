package http

import "gitlab.com/darleneeon/journal"

// Page represents the variables required for each page.
type Page struct {
	Title   string
	MenuURL string
}

// MenuPage represents the variables required for the 'Menu' page.
type MenuPage struct {
	Page
	CreateNoteURL string
	SavedNotesURL string
	SettingsURL   string
}

// SettingsPage represents the variables required for the 'Settings' page.
type SettingsPage struct {
	Page
	Form        journal.PasswordForm
	SettingsURL string
}

// AuthorizationPage represents the variables required for the 'Authorization' page.
type AuthorizationPage struct {
	Page
	Form             journal.AuthorizationForm
	AuthorizationURL string
}

// CreateNotePage represents the variables required for the 'Create note' page.
type CreateNotePage struct {
	Page
	Form          journal.NoteForm
	CreateNoteURL string
}

// SavedNotesPage represents the variables required for the 'Saved notes' page.
type SavedNotesPage struct {
	Page
	Notes []journal.Note
}

// NotePage represents the variables required for the 'Note' page.
type NotePage struct {
	Page
	NoteDate      string
	Paragraphs    []string
	EditAllowed   bool
	EditNoteURL   string
	DeleteNoteURL string
}
