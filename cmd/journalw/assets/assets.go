package assets

import (
	"fmt"
	"os"
	"path/filepath"

	"gitlab.com/darleneeon/journal"
)

// Path to the project in GOPATH
const project = "gitlab.com/darleneeon/journal/cmd/journalw"

// GOPATH errors
const (
	ErrGOPATHIsNotSet     = journal.Error("GOPATH is not set")
	ErrGOPATHDoesNotExist = journal.Error("GOPATH directory does not exist")
)

// ProjectPath returns the path to the project directory.
func ProjectPath() (string, error) {
	// Get and check GOPATH
	gopath := os.Getenv("GOPATH")
	if len(gopath) == 0 {
		return "", ErrGOPATHIsNotSet
	} else if _, err := os.Stat(gopath); os.IsNotExist(err) {
		return "", ErrGOPATHDoesNotExist
	}

	// Generate path to the project
	path := filepath.Clean(filepath.FromSlash(fmt.Sprintf("%s/src/%s", gopath, project)))
	if _, err := os.Stat(path); os.IsNotExist(err) {
		return "", err
	}

	return path, nil
}

// TemplatesPath returns the path to the templates direcotry.
func TemplatesPath() (string, error) {
	return assetsPath("templates")
}

// StaticPath returns the path to the static directory.
func StaticPath() (string, error) {
	return assetsPath("static")
}

func assetsPath(dir string) (string, error) {
	projectPath, err := ProjectPath()
	if err != nil {
		return "", err
	}

	path := filepath.FromSlash(fmt.Sprintf("%s/assets/%s", projectPath, dir))
	if _, err := os.Stat(path); os.IsNotExist(err) {
		return "", err
	}

	return path, nil
}
