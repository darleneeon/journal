package main

import (
	"fmt"
	"os/user"
	"path/filepath"

	"gitlab.com/darleneeon/journal/bolt"
	"gitlab.com/darleneeon/journal/cmd/journalw/http"
)

func main() {
	usr, err := user.Current()
	if err != nil {
		panic(err)
	}

	client := bolt.NewClient()
	client.Path = filepath.Join(usr.HomeDir, ".journal.db")

	if err := client.Open(); err != nil {
		panic(err)
	}
	defer client.Close()

	app := http.NewApp(true)
	app.Handler.NoteService = client.NoteService()
	app.Handler.SettingsService = client.SettingsService()
	app.Handler.EncryptionService = client.EncryptionService()

	if err := app.Start(); err != nil {
		panic(err)
	}
	defer app.Stop()

	app.Log.Printf("Run HTTP server at http://localhost%s/", app.Server.Addr)

	fmt.Scanln()
}
