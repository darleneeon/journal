package journal

import "strings"

// Form represents a journal form.
type Form interface {
	Validate() error
}

// NoteForm represents a form of note creation.
type NoteForm struct {
	Text    string
	TextErr string
}

// Validate returns a result of NoteForm validation.
func (f *NoteForm) Validate() error {
	f.Text = strings.TrimSpace(f.Text)

	if len(f.Text) == 0 {
		f.TextErr = ErrNoteTextRequired.Error()

		return ErrNoteTextRequired
	}

	return nil
}

// PasswordForm represents a form of password creation / changing.
type PasswordForm struct {
	Change bool

	Password    string
	PasswordErr string

	NewPassword    string
	NewPasswordErr string
}

// Validate returns a result of PasswordForm validation.
func (f *PasswordForm) Validate() error {
	f.Password = strings.TrimSpace(f.Password)
	f.NewPassword = strings.TrimSpace(f.NewPassword)

	if f.Change {
		if len(f.Password) == 0 && len(f.NewPassword) == 0 {
			f.PasswordErr = ErrPasswordRequired.Error()
			f.NewPasswordErr = ErrNewPasswordRequired.Error()

			return ErrPasswordsRequired
		} else if len(f.Password) == 0 {
			f.PasswordErr = ErrPasswordRequired.Error()

			return ErrPasswordRequired
		} else if len(f.NewPassword) == 0 {
			f.NewPasswordErr = ErrNewPasswordRequired.Error()

			return ErrNewPasswordRequired
		} else if f.Password == f.NewPassword {
			f.NewPasswordErr = ErrEqualPasswords.Error()

			return ErrEqualPasswords
		}
	} else if len(f.NewPassword) == 0 {
		f.NewPasswordErr = ErrPasswordRequired.Error()

		return ErrPasswordRequired
	}

	return nil
}

// AuthorizationForm represents a form authorization.
type AuthorizationForm struct {
	Password    string
	PasswordErr string
}

// Validate returns a result of AuthorizationForm validation.
func (f *AuthorizationForm) Validate() error {
	f.Password = strings.TrimSpace(f.Password)

	if len(f.Password) == 0 {
		f.PasswordErr = ErrPasswordRequired.Error()

		return ErrPasswordRequired
	}

	return nil
}
